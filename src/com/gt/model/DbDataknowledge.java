package com.gt.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "db_dataknowledge", schema = "layssh", catalog = "")
@IdClass(DbDataknowledgePK.class)
public class DbDataknowledge {
    private int id;
    private String dataTitle;
    private String dataContent;
    private String pushTime;
    private String author;
    private String knoeleageType;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "data_title")
    public String getDataTitle() {
        return dataTitle;
    }

    public void setDataTitle(String dataTitle) {
        this.dataTitle = dataTitle;
    }

    @Basic
    @Column(name = "data_content")
    public String getDataContent() {
        return dataContent;
    }

    public void setDataContent(String dataContent) {
        this.dataContent = dataContent;
    }

    @Basic
    @Column(name = "push_time")
    public String getPushTime() {
        return pushTime;
    }

    public void setPushTime(String pushTime) {
        this.pushTime = pushTime;
    }

    @Basic
    @Column(name = "author")
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Id
    @Column(name = "knoeleage_type")
    public String getKnoeleageType() {
        return knoeleageType;
    }

    public void setKnoeleageType(String knoeleageType) {
        this.knoeleageType = knoeleageType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DbDataknowledge that = (DbDataknowledge) o;
        return id == that.id &&
                Objects.equals(dataTitle, that.dataTitle) &&
                Objects.equals(dataContent, that.dataContent) &&
                Objects.equals(pushTime, that.pushTime) &&
                Objects.equals(author, that.author) &&
                Objects.equals(knoeleageType, that.knoeleageType);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, dataTitle, dataContent, pushTime, author, knoeleageType);
    }
}
