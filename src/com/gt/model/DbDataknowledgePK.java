package com.gt.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class DbDataknowledgePK implements Serializable {
    private int id;
    private String knoeleageType;

    @Column(name = "id")
    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "knoeleage_type")
    @Id
    public String getKnoeleageType() {
        return knoeleageType;
    }

    public void setKnoeleageType(String knoeleageType) {
        this.knoeleageType = knoeleageType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DbDataknowledgePK that = (DbDataknowledgePK) o;
        return id == that.id &&
                Objects.equals(knoeleageType, that.knoeleageType);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, knoeleageType);
    }
}
