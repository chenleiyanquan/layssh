package com.gt.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "eai_case", schema = "layssh", catalog = "")
public class EaiCase {
    private int caseId;
    private String caseTitle;
    private String caseName;
    private String pushTime;

    @Id
    @Column(name = "case_id")
    public int getCaseId() {
        return caseId;
    }

    public void setCaseId(int caseId) {
        this.caseId = caseId;
    }

    @Basic
    @Column(name = "case_title")
    public String getCaseTitle() {
        return caseTitle;
    }

    public void setCaseTitle(String caseTitle) {
        this.caseTitle = caseTitle;
    }

    @Basic
    @Column(name = "case_name")
    public String getCaseName() {
        return caseName;
    }

    public void setCaseName(String caseName) {
        this.caseName = caseName;
    }

    @Basic
    @Column(name = "push_time")
    public String getPushTime() {
        return pushTime;
    }

    public void setPushTime(String pushTime) {
        this.pushTime = pushTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EaiCase eaiCase = (EaiCase) o;
        return caseId == eaiCase.caseId &&
                Objects.equals(caseTitle, eaiCase.caseTitle) &&
                Objects.equals(caseName, eaiCase.caseName) &&
                Objects.equals(pushTime, eaiCase.pushTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(caseId, caseTitle, caseName, pushTime);
    }
}
