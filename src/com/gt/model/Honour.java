package com.gt.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Honour {
    private int id;
    private String honourName;
    private String honContent;
    private String gainTime;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "honour_name")
    public String getHonourName() {
        return honourName;
    }

    public void setHonourName(String honourName) {
        this.honourName = honourName;
    }

    @Basic
    @Column(name = "hon_content")
    public String getHonContent() {
        return honContent;
    }

    public void setHonContent(String honContent) {
        this.honContent = honContent;
    }

    @Basic
    @Column(name = "gain_time")
    public String getGainTime() {
        return gainTime;
    }

    public void setGainTime(String gainTime) {
        this.gainTime = gainTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Honour honour = (Honour) o;
        return id == honour.id &&
                Objects.equals(honourName, honour.honourName) &&
                Objects.equals(honContent, honour.honContent) &&
                Objects.equals(gainTime, honour.gainTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, honourName, honContent, gainTime);
    }
}
