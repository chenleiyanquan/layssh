package com.gt.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class About {
    private int id;
    private String aboutType;
    private String aboutContent;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "about_type")
    public String getAboutType() {
        return aboutType;
    }

    public void setAboutType(String aboutType) {
        this.aboutType = aboutType;
    }

    @Basic
    @Column(name = "about_content")
    public String getAboutContent() {
        return aboutContent;
    }

    public void setAboutContent(String aboutContent) {
        this.aboutContent = aboutContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        About about = (About) o;
        return id == about.id &&
                Objects.equals(aboutType, about.aboutType) &&
                Objects.equals(aboutContent, about.aboutContent);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, aboutType, aboutContent);
    }
}
