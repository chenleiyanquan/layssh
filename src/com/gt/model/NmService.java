package com.gt.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "nm_service", schema = "layssh", catalog = "")
public class NmService {
    private int id;
    private String serviceName;
    private String serviceContent;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "service_name")
    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    @Basic
    @Column(name = "service_content")
    public String getServiceContent() {
        return serviceContent;
    }

    public void setServiceContent(String serviceContent) {
        this.serviceContent = serviceContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NmService nmService = (NmService) o;
        return id == nmService.id &&
                Objects.equals(serviceName, nmService.serviceName) &&
                Objects.equals(serviceContent, nmService.serviceContent);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, serviceName, serviceContent);
    }
}
