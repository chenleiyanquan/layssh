package com.gt.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Businessrange {
    private int id;
    private String buisName;
    private String buisContent;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "buis_name")
    public String getBuisName() {
        return buisName;
    }

    public void setBuisName(String buisName) {
        this.buisName = buisName;
    }

    @Basic
    @Column(name = "buis_content")
    public String getBuisContent() {
        return buisContent;
    }

    public void setBuisContent(String buisContent) {
        this.buisContent = buisContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Businessrange that = (Businessrange) o;
        return id == that.id &&
                Objects.equals(buisName, that.buisName) &&
                Objects.equals(buisContent, that.buisContent);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, buisName, buisContent);
    }
}
