package com.gt.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "eai_user", schema = "layssh", catalog = "")
public class EaiUser {
    private int userId;
    private String userName;
    private String passWord;

    @Id
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "user_name")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "pass_word")
    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EaiUser eaiUser = (EaiUser) o;
        return userId == eaiUser.userId &&
                Objects.equals(userName, eaiUser.userName) &&
                Objects.equals(passWord, eaiUser.passWord);
    }

    @Override
    public int hashCode() {

        return Objects.hash(userId, userName, passWord);
    }
}
