package com.gt.pageModel;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @功能说明：系统用户
 * @作者： herun
 * @创建日期：2015-09-19
 * @版本号：V1.0
 */
public class NewsModel extends BasePageForLayUI {

	private static final long serialVersionUID = 1L;
    private int newsId;
    private String newsTitle;
    private String newsName;
    private String pushTime;
    private String newsType;
	public int getNewsId() {
		return newsId;
	}
	public void setNewsId(int newsId) {
		this.newsId = newsId;
	}
	public String getNewsTitle() {
		return newsTitle;
	}
	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}
	public String getNewsName() {
		return newsName;
	}
	public void setNewsName(String newsName) {
		this.newsName = newsName;
	}
	public String getPushTime() {
		return pushTime;
	}
	public void setPushTime(String pushTime) {
		this.pushTime = pushTime;
	}
	public String getNewsType() {
		return newsType;
	}
	public void setNewsType(String newsType) {
		this.newsType = newsType;
	}
    
}
