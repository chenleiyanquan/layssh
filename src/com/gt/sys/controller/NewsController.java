package com.gt.sys.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gt.model.News;
import com.gt.pageModel.Json;
import com.gt.pageModel.MenuInf;
import com.gt.pageModel.NewsModel;
import com.gt.sys.service.INewsService;

/**
 * @功能说明：菜单功能
 * @作者： herun
 * @创建日期：2015-09-24
 * @版本号：V1.0
 */
@Controller
@RequestMapping("/news")
public class NewsController extends BaseController {

	private Logger logger = Logger.getLogger(NewsController.class);
	@Autowired
	private INewsService newService;
	/**
	 * 新增
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Json add(NewsModel news, HttpServletRequest request, HttpSession session) {
		Json j = new Json();
		try {
				NewsModel m = newService.add(news);
				j.setSuccess(true);
				j.setMsg("添加成功！");
				j.setObj(m);

		} catch (Exception e) {
			logger.error(e.getMessage());
			j.setSuccess(false);
			j.setMsg("添加失败:" + e.getMessage());
		}
		// 添加系统日志
		writeSysLog(j, news, request, session);

		return j;
	}
}
