package com.gt.sys.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.gt.model.News;
import com.gt.model.TSysMenuInf;
import com.gt.pageModel.DatagridForLayUI;
import com.gt.pageModel.Json;
import com.gt.pageModel.MenuForLayUI;
import com.gt.pageModel.MenuInf;
import com.gt.pageModel.NewsModel;
import com.gt.pageModel.TreeForLayUI;
import com.gt.pageModel.ZTree;
import com.gt.sys.service.IMenuInfService;
import com.gt.sys.service.INewsService;
import com.gt.sys.service.ISysFunctionInfService;
import com.gt.utils.DataConverter;
import com.gt.utils.MapToBeanUtils;
import com.gt.utils.PbUtils;
import com.gt.utils.ReadFile;

/**
 * 
 * @功能说明：菜单功能service实现
 * @作者： herun
 * @创建日期：2015-09-23
 * @版本号：V1.0
 */
@Service("newService")
public class NewsServiceImpl extends BaseServiceImpl<News> implements INewsService {

	@Override
	public List<News> getNews(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<News> getAllNews() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DatagridForLayUI datagrid(NewsModel news) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NewsModel add(NewsModel newsModle) {
		News news = new News();
		BeanUtils.copyProperties(newsModle, news);
		save(news);
		BeanUtils.copyProperties(news,newsModle);
		return newsModle;
	}
	
	@Override
	public void remove(String ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Json modify(NewsModel news) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
