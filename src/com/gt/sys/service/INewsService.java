package com.gt.sys.service;

import java.util.List;

import com.gt.model.News;
import com.gt.pageModel.DatagridForLayUI;
import com.gt.pageModel.Json;
import com.gt.pageModel.NewsModel;

/**
 * 
 * @功能说明：菜单功能service接口
 * @作者： herun
 * @创建日期：2015-09-22
 * @版本号：V1.0
 */
public interface INewsService extends IBaseService<News> {

	/**
	 * 异步加载树
	 * 
	 * @param id
	 * @return
	 */
	public List<News> getNews(String id);

	/**
	 * 加载全部树
	 * 
	 * @return
	 */
	public List<News> getAllNews();

	/**
	 * 获取datagrid数据
	 * 
	 * @return
	 */
	public DatagridForLayUI datagrid(NewsModel news);

	/**
	 * 新增
	 * 
	 * @param News
	 * @return
	 */
	public NewsModel add(NewsModel news);

	/**
	 * 删除
	 * 
	 * @param ids
	 */
	public void remove(String ids);

	/**
	 * 修改
	 * 
	 * @param News
	 */
	public Json modify(NewsModel news);

}
